/* eslint-disable no-underscore-dangle */
/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
/* eslint-disable indent */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-undef */
/* eslint-disable no-plusplus */
import { randomUserMock, additionalUsers } from '../mock_for_L3';

require('../css/app.css');

/** ******** Your code here! *********** */

const users_ = getFormattedUsers();
console.log(users_);

console.log(validate(users_[0]));

console.log(filterByCountry('Norway'));

console.log(filterByAge(40));

sortByName();
console.log(users_);

sortByAge();
console.log(users_);

console.log(findByName('Mario'));

console.log(getAgePercentage(40));

function getFormattedUsers() {
    // eslint-disable-next-line no-shadow
    const users = [];
    for (let i = 0; i < randomUserMock.length; i++) {
        const user = randomUserMock[i];
        users.push({
            gender: user.gender,
            title: user.name.title,
            full_name: `${user.name.first} ${user.name.last}`,
            city: user.location.city,
            state: user.location.state,
            country: user.location.country,
            postcode: user.location.postcode,
            coordinates: user.location.coordinates,
            timezone: user.location.timezone,
            email: user.email,
            b_date: user.dob.date,
            age: user.dob.age,
            phone: user.phone,
            picture_large: user.picture.large,
            picture_thumbnail: user.picture.thumbnail,
            id: user.id.value,
            favorite: false,
            course: null,
            bg_color: null,
            note: null,
        });
    }

    for (let i = 0; i < additionalUsers.length; i++) {
        const additionalUser = additionalUsers[i];
        let flag = false;
        for (let j = 0; j < users.length; j++) {
            if (users[j].full_name === additionalUser.full_name) {
                for (const key in additionalUser) {
                    if (additionalUser[key] != null) {
                        users[j][key] = additionalUser[key];
                    }
                }
                flag = true;
                break;
            }
        }
        if (!flag && validate(additionalUser)) {
            users.push(additionalUser);
        }
    }

    return users;
}

// eslint-disable-next-line no-unused-vars
function validate(user) {
    let flag = false;
    if (typeof user.full_name === 'string' && startsWithCapital(user.full_name)
        && typeof user.gender === 'string'
        // && startsWithCapital(user.gender)
        // && typeof user.note === 'string' && startsWithCapital(user.note)
        && typeof user.state === 'string' && startsWithCapital(user.state)
        && typeof user.city === 'string' && startsWithCapital(user.city)
        && typeof user.country === 'string' && startsWithCapital(user.country)
        && typeof user.age === 'number'
        && validateEmail(user.email)) {
        flag = true;
    }
    return flag;
}

function startsWithCapital(word) {
    return word.charAt(0) === word.charAt(0).toUpperCase();
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function filterByCountry(country) {
    return users_.filter((user) => user.country === country);
}

function filterByAge(age) {
    return users_.filter((user) => user.age >= age);
}

function sortByName() {
    users_.sort((a, b) => {
        if (a.full_name < b.full_name) { return -1; }
        if (a.full_name > b.full_name) { return 1; }
        return 0;
    });
}

function sortByAge() {
    users_.sort((a, b) => {
        if (a.age < b.age) { return -1; }
        if (a.age > b.age) { return 1; }
        return 0;
    });
}

function findByName(name) {
    const results = [];
    users_.forEach((user) => {
        if (user.full_name.includes(name)) {
            results.push(user);
        }
    });
    return results;
}

function getAgePercentage(age) {
    // eslint-disable-next-line prefer-destructuring
    const length = users_.filter((user) => user.age >= age).length;
    return Math.round((length / users_.length) * 100);
}
